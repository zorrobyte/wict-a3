/*
__          __        _     _   _          _____             __ _ _      _     _______          _ 
\ \        / /       | |   | | (_)        / ____|           / _| (_)    | |   |__   __|        | |
 \ \  /\  / /__  _ __| | __| |  _ _ __   | |     ___  _ __ | |_| |_  ___| |_     | | ___   ___ | |
  \ \/  \/ / _ \| '__| |/ _` | | | '_ \  | |    / _ \| '_ \|  _| | |/ __| __|    | |/ _ \ / _ \| |
   \  /\  / (_) | |  | | (_| | | | | | | | |___| (_) | | | | | | | | (__| |_     | | (_) | (_) | |
    \/  \/ \___/|_|  |_|\__,_| |_|_| |_|  \_____\___/|_| |_|_| |_|_|\___|\__|    |_|\___/ \___/|_|

  ,---.                     ,---.  ,--.,--.,--.            ,--.,--.      
 /  O  \ ,--.--.,--,--,--. /  O  \ |  ||  ||  ,---.  ,---. |  |`--' ,---.
|  .-.  ||  .--'|        ||  .-.  ||  ||  ||  .-.  || .-. ||  |,--.| .--'
|  | |  ||  |   |  |  |  ||  | |  ||  ||  ||  | |  |' '-' '|  ||  |\ `--.
`--' `--'`--'   `--`--`--'`--' `--'`--'`--'`--' `--' `---' `--'`--' `---'
*/

if (isServer) then
{
	/* This is a place for user defined settings that WICT loads at the beginning. */

	/* Limit in the main loop in the AutoFlag function =  maximum number of the bases of ANY TYPE */
	WICT_flagLoopLimit = 999;
	
	/* Auto Flag function.
	Parameter(s):
		1: base type (any string you want : "base1","base2","infantry","lightVehicles","tanks" etc.)
		2: radius of the trigger
		3: trigger timeout in form [min, mid, max]
	Note this for multiplayer: 
	Trigger will only be present on the machine(s) this command is run = server! */
	_null = ["regInfantry",30,[1,2,5]] execVM "WICT\autoFlag.sqf";
	_null = ["specInfantry",30,[1,2,5]] execVM "WICT\autoFlag.sqf";
	_null = ["lightVehicles",35,[2,4,6]] execVM "WICT\autoFlag.sqf";
	_null = ["mediumVehicles",35,[2,4,6]] execVM "WICT\autoFlag.sqf";
	_null = ["mediumArmor",40,[3,5,8]] execVM "WICT\autoFlag.sqf";
	_null = ["heavyArmor",40,[3,5,8]] execVM "WICT\autoFlag.sqf";
	_null = ["airCavalry",40,[3,5,8]] execVM "WICT\autoFlag.sqf";
	_null = ["allClasses",30,[1,2,6]] execVM "WICT\autoFlag.sqf";
	
	
	/* Here you can set how long one turn lasts. 
	I suggest you to read the manual first and then decide will you change these values. 
	There are other ways for changing spawning frequency. */
	WICT_time = 2.5;
	WICT_timeRand = 5;

	/* Do not change spawning distance if you are not sure what you are doing. 
	The upper limit is WICT_scandist. Although there is no lower limit except 0, 
	decreasing spawning distance is only for zombie mode ;) */
	WICT_sd = 250;
	WICT_sdRand = 100;
	/* Additionally you can set up fixed displacement for aircrafts that will increase spawn distance. */
	WICT_displace = 350;

	/* This is scan distance for searching nearest bases. Bases further than this distance won't be considered during 
	searching and tactical planning.
	Default value is 1km.
	Scan distance for neutral sectors will be WICT_scandist + 500m. 
	Note that base won't be consider either if you are closer than spawning distance limit WICT_sd. 
	In other words base will be active if:
		1) it is nearest
		2) it is further than WICT_sd
		3) it is closer than WICT_scandist */
	WICT_scandist = 1000;
	
	/* Let's remove some "far, far away" units (in meters) */
	WICT_removeMan = 2500;
	/* Let's remove some "far, far away" vehicles (in meters) */
	WICT_removeVehicle = 3000;
	/* Remove body (in seconds) */
	WICT_removeBody = 180;

	/* Here you can set number of AI groups - please read the manual before setting this. */
	WICT_numAIg = 55;
	/* Here you can set Anti-Jam -- again read the manual */
	WICT_jam = 65;

	/* This is debug mode:
	Show from which base spawning process took place and show spawning marker as "warning" sign
	-- this debug yes/no is required for core files. */
	WICT_debug = "yes";
	
/*
oooooo   oooooo     oooo ooooo   .oooooo.   ooooooooooooo                                   oooo 
 `888.    `888.     .8'  `888'  d8P'  `Y8b  8'   888   `8                                   `888 
  `888.   .8888.   .8'    888  888               888         oo.ooooo.   .ooooo.   .ooooo.   888 
   `888  .8'`888. .8'     888  888               888          888' `88b d88' `88b d88' `88b  888 
    `888.8'  `888.8'      888  888               888          888   888 888   888 888   888  888 
     `888'    `888'       888  `88b    ooo       888          888   888 888   888 888   888  888 
      `8'      `8'       o888o  `Y8bood8P'      o888o         888bod8P' `Y8bod8P' `Y8bod8P' o888o
                                                              888                                
                                                             o888o                               
             _ _          ___               _     _      _           
 _   _ _ __ (_) |_ ___   ( _ )   __   _____| |__ (_) ___| | ___  ___ 
| | | | '_ \| | __/ __|  / _ \/\ \ \ / / _ \ '_ \| |/ __| |/ _ \/ __|
| |_| | | | | | |_\__ \ | (_>  <  \ V /  __/ | | | | (__| |  __/\__ \
 \__,_|_| |_|_|\__|___/  \___/\/   \_/ \___|_| |_|_|\___|_|\___||___/
                                                                     

   ___  __   __  __________  ___ 
  / _ )/ /  / / / / __/ __ \/ _ \
 / _  / /__/ /_/ / _// /_/ / , _/
/____/____/\____/_/  \____/_/|_|       
*/

	/* Regular infantry, soldiers, medics, leaders and officers */
	wict_w_inf = [
	["B_Soldier_TL_F","B_Soldier_F","B_Soldier_F","B_medic_F"],
	["B_Soldier_TL_F","B_Soldier_F","B_medic_F","B_medic_F"],
	["B_Soldier_TL_F","B_medic_F","B_medic_F","B_medic_F"],
	["B_Soldier_TL_F","B_Soldier_F","B_soldier_UAV_F","B_medic_F"]
	];

	/* Machinegunners and grenadiers */
	wict_w_support = [
	["B_Soldier_TL_F","B_soldier_AR_F","B_Soldier_GL_F","B_soldier_AR_F"]
	];

	/* Snipers */
	wict_w_snip = [
	["B_soldier_UAV_F","B_soldier_M_F"]
	]; 

	/* Marksmen and spotters (SpecOp) */
	wict_w_spec = [
	["B_Soldier_TL_F","B_soldier_AR_F","B_soldier_UAV_F","B_soldier_M_F"]
	];

	/* AT units */
	wict_w_at = [
	["B_Soldier_TL_F","B_soldier_LAT_F","B_soldier_AT_F","B_soldier_AA_F"]
	];

	/* Jeeps and light vehicles -- the first element is ARRAY with vehicle(s) */
	wict_w_lightveh = [
	[["B_MRAP_01_hmg_F"],"B_Soldier_TL_F","B_Soldier_F","B_Soldier_F","B_Soldier_F","B_medic_F"],
	[["B_MRAP_01_F"],"B_Soldier_TL_F","B_Soldier_F","B_Soldier_F","B_Soldier_F","B_medic_F"],
	[["B_MRAP_01_gmg_F"],"B_Soldier_TL_F","B_Soldier_F","B_Soldier_F","B_Soldier_F","B_medic_F"]
	];

	/* Transport -- the first element is ARRAY with vehicle(s) */
	wict_w_trans = [
	[["B_Truck_01_covered_F"],"B_Soldier_TL_F","B_Soldier_F","B_Soldier_F","B_Soldier_F","B_medic_F","B_Soldier_TL_F","B_soldier_LAT_F","B_soldier_AT_F","B_soldier_M_F","USMC_Soldier_AA","B_soldier_UAV_F","B_soldier_M_F","B_soldier_AR_F"],
	[["B_Heli_Transport_01_F"],"B_Soldier_TL_F","B_Soldier_F","B_Soldier_F","B_Soldier_F","B_medic_F","B_Soldier_TL_F","B_soldier_M_F","B_soldier_AT_F","B_soldier_LAT_F"],
	[["B_MRAP_01_hmg_F","B_MRAP_01_F"],"B_Soldier_TL_F","B_Soldier_F","B_Soldier_F","B_Soldier_F","B_medic_F","B_Soldier_TL_F","B_Soldier_F","B_Soldier_F","B_medic_F"],
	[["B_MRAP_01_F","B_MRAP_01_F"],"B_Soldier_TL_F","B_Soldier_F","B_medic_F","B_medic_F","B_medic_F","B_Soldier_TL_F","B_Soldier_F","B_Soldier_F","B_medic_F"],
	[["B_MRAP_01_hmg_F","B_MRAP_01_F"],"B_Soldier_TL_F","B_Soldier_F","B_Soldier_F","B_medic_F","B_medic_F","B_Soldier_TL_F","B_Soldier_F","B_Soldier_F","B_medic_F"]
	];

	/* Infantry fighting vehicles -- the first element is ARRAY with vehicle(s) */
	wict_w_ifv = [
	[["B_APC_Wheeled_01_cannon_F"],"B_Soldier_TL_F","B_Soldier_F","B_Soldier_F","B_soldier_LAT_F","B_medic_F"]
	];

	/* Medium and light tanks -- the first element is ARRAY with vehicle(s) */
	wict_w_mtank = [
	[["B_APC_Tracked_01_rcws_F"],"B_Soldier_TL_F","B_Soldier_F","B_Soldier_F"]
	];

	/* Heavy tanks -- the first element is ARRAY with vehicle(s) */
	wict_w_htank = [
	[["B_MBT_01_cannon_F"],"B_Soldier_TL_F","B_Soldier_F","B_Soldier_F"]
	];

	/* Medium and light choppers -- the first element is ARRAY with vehicle(s) */
	wict_w_mchop = [
	[["B_Heli_Light_01_armed_F"],"B_Pilot_F","B_crew_F"]
	];

	/* Heavy choppers -- the first element is ARRAY with vehicle(s) */
	wict_w_hchop = [
	[["B_Heli_Attack_01_F"],"B_Pilot_F","B_crew_F"]
	];
	
	/* Airplanes -- the first element is ARRAY with vehicle(s) */
	wict_w_air = [
	[["B_Plane_CAS_01_F"],"B_Helipilot_F"]
	];

/*
  ____  ___  ________  ___ 
 / __ \/ _ \/ __/ __ \/ _ \
/ /_/ / ___/ _// /_/ / , _/
\____/_/  /_/  \____/_/|_| 
*/
	/* Regular infantry, soldiers, medics, leaders and officers */
	wict_e_inf = [
	["O_Soldier_TL_F","O_Soldier_F","O_Soldier_F","O_medic_F"],
	["O_Soldier_TL_F","O_Soldier_F","RU_Soldier_Marksman","O_medic_F"],
	["O_Soldier_TL_F","O_Soldier_F","O_medic_F","O_medic_F"],
	["O_Soldier_TL_F","O_medic_F","O_medic_F","O_medic_F"]
	];
	
	/* Machinegunners and grenadiers */
	wict_e_support = [
	["O_Soldier_TL_F","O_Soldier_AR_F","O_Soldier_GL_F","O_Soldier_AR_F"]
	];
	
	/* Snipers */
	wict_e_snip = [
	["O_soldier_UAV_F","O_soldier_M_F"]
	];
	
	/* Marksmen and spotters (SpecOp) */
	wict_e_spec = [
	["O_Soldier_TL_F","O_Soldier_AR_F","O_soldier_UAV_F","O_soldier_M_F"]
	];
	
	/* AT units */
	wict_e_at = [
	["O_Soldier_TL_F","O_Soldier_LAT_F","O_Soldier_AT_F","O_Soldier_AA_F"]
	];
	
	/* Jeeps and light vehicles -- the first element is ARRAY with vehicle(s) */
	wict_e_lightveh = [
	[["O_MRAP_02_hmg_F"],"O_Soldier_TL_F","O_Soldier_F","O_Soldier_F","O_Soldier_F","O_medic_F"],
	[["O_MRAP_02_F"],"O_Soldier_TL_F","O_Soldier_F","O_Soldier_F","O_Soldier_F","O_medic_F"],
	[["O_MRAP_02_gmg_F"],"O_Soldier_TL_F","O_Soldier_F","O_Soldier_F","O_Soldier_F","O_medic_F"]
	];
	
	/* Transport -- the first element is ARRAY with vehicle(s) */
	wict_e_trans = [
	[["O_Truck_02_covered_F"],"O_Soldier_TL_F","O_Soldier_F","O_Soldier_F","O_Soldier_F","O_medic_F","O_soldier_UAV_F","O_soldier_M_F","O_Soldier_AR_F"],
	[["O_Heli_Light_02_F"],"O_Soldier_TL_F","O_Soldier_F","O_Soldier_F","O_Soldier_F","O_medic_F","O_Soldier_TL_F","O_soldier_M_F","O_soldier_AT_F","O_soldier_LAT_F"],	
	[["O_MRAP_02_gmg_F","O_MRAP_02_F"],"O_Soldier_TL_F","O_Soldier_F","O_Soldier_F","O_Soldier_F","O_medic_F","O_Soldier_TL_F","O_Soldier_F","O_Soldier_F","O_medic_F"],
	[["O_MRAP_02_F","O_MRAP_02_F"],"O_Soldier_TL_F","O_Soldier_F","O_Soldier_F","O_Soldier_F","O_medic_F","O_Soldier_TL_F","O_Soldier_F","O_Soldier_F","O_medic_F"],
	[["O_MRAP_02_hmg_F","O_MRAP_02_F"],"O_Soldier_TL_F","O_Soldier_F","O_Soldier_F","O_Soldier_F","O_medic_F","O_Soldier_TL_F","O_Soldier_F","O_Soldier_F","O_medic_F"]
	];
	
	/* Infantry fighting vehicles -- the first element is ARRAY with vehicle(s) */
	wict_e_ifv = [
	[["O_APC_Wheeled_02_rcws_F"],"O_Soldier_TL_F","O_Soldier_F","O_Soldier_F","O_soldier_LAT_F","O_medic_F"]
	];
	
	/* Medium and light tanks -- the first element is ARRAY with vehicle(s) */
	wict_e_mtank = [
	[["O_APC_Tracked_02_cannon_F"],"O_Soldier_TL_F","O_Soldier_F","O_Soldier_F"]
	];
	
	/* Heavy tanks -- the first element is ARRAY with vehicle(s) */
	wict_e_htank = [
	[["O_MBT_02_cannon_F"],"O_Soldier_TL_F","O_Soldier_F","O_Soldier_F"]
	];

	/* Medium and light choppers -- the first element is ARRAY with vehicle(s) */
	wict_e_mchop = [
	[["O_Heli_Attack_02_F"],"O_Helipilot_F","O_crew_F"]
	];

	/* Heavy choppers -- the first element is ARRAY with vehicle(s) */
	wict_e_hchop = [
	[["O_Heli_Attack_02_black_F"],"O_Helipilot_F","O_crew_F"]
	];
	
	/* Airplanes -- the first element is ARRAY with vehicle(s) */
	wict_e_air = [
	[["O_Plane_CAS_02_F"],"O_Pilot_F"]
	];
};