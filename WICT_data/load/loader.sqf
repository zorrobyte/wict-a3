//{}{}{}{}{}{}{}{}{} LOADER {}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}{}
//waituntil {!isnil "bis_fnc_init"};
//waituntil {BIS_MPF_InitDone};


//{}{}{}{}{}{}{}{}{} LOAD DATA FROM CLIPBOARD TO SERVER {}{}{}{}{}{}{}
if(isServer) then 
{
	// put data from Clipboard before semicolumn mark (replace *** with content of Clipboard)
	// repeat process for every captured mission!!!
	// this is example of syntax >>> { _null = [_x] execVM "WICT\sandbox\murk_spawn_loaded.sqf"; } forEach *** ;
};

//{}{}{}{}{}{}{}{}{} GIVING TASK AT BEGINNING {}{}{}{}{}{}{}{}{}{}{}{}
// Dedicated server doesn't have a player, ever!
if (!isDedicated) then 
{
	// make sure player object exists i.e. not JIP
	waitUntil {!isNull player};
	waitUntil {player == player};
	
	sleep 3;

	//ZBE[nil,nil,rEXECVM,"WICT\sandbox\sandbox_exe.sqf","WICT\sandbox\","taskCreator","Airport base","mixedBase_1","yes","Primary: capture enemy base!","Intel: <br/>Capture the enemy base at the airport.<br/><br/>Background info:<br/>put some text here -- use Briefing manager to easily write the text and of course Word Wrap in your text editor :) be creative :D","yes","all"] call RE;
};

//{}{}{} CALLING SUPPLY DROP AND REINFORCEMENTS AT BEGINNING {}{}{}{}{}
if (isServer) then
{	
	//ZBE[nil,nil,rEXECVM,"WICT\sandbox\sandbox_exe.sqf","WICT\support\","supplyDrop",10,position player, "C130J",(round(random 360)), 1000, ["USBasicAmmunitionBox","USLaunchersBox","HMMWV","MTVR"],"west",false,"arma",false,2,8,1] call RE;
	
	sleep 5;
	//ZBE[position player, "C130J", (round(random 360)), 1000, ["USMC_Soldier_Medic", "USMC_Soldier_Medic","USMC_Soldier_Medic","USMC_Soldier_Medic","USMC_Soldier_AT","USMC_SoldierM_Marksman","USMC_Soldier_MG","USMC_Soldier_AA"],false,true] execVM "WICT\support\reinforce.sqf";
};